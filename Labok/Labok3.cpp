#include <iostream>
#include <cmath>
#include <new>
#include <cstring>
#include <conio.h>
#include <vector>
#include <list>


using namespace std;

/*#define QMAX 50
struct queue
{
    int qu[QMAX];
    int rear, frnt;
};

void init(queue* q)
{
    q->frnt = 1;
    q->rear = 0;
    return;
}

void insert(queue* q, int x)
{
    if (q->rear < QMAX - 1)
    {
        q->rear++;
        q->qu[q->rear] = x;
    }
    else
        printf("������� �����!\n");
    return;
}

int isempty(queue* q)
{
    if (q->rear < q->frnt)
    {
        //cout << "������." << endl;
        return 0;
    }
    else
    {
        //cout << "�� ������." << endl;
        return 1;
    }
}

void print(queue* q)
{
    int h;
    if (isempty(q) == 0)
    {
        printf("������� �����!\n");
        return;
    }
    for (h = q->frnt; h <= q->rear; h++)
        printf("%d ", q->qu[h]);
    return;
}

int remove(queue* q)
{
    int x, h;
    if (isempty(q) == 0)
    {
        printf("������� �����!\n");
        return(0);
    }
    x = q->qu[q->frnt];
    for (h = q->frnt; h < q->rear; h++)
    {
        q->qu[h] = q->qu[h + 1];
    }
    q->rear--;
    return x;
}

void size(queue* q)
{
    cout << "������� ����� ��������� � �������: " << q->rear << endl;
}

void clear(queue* q)
{
    free(q);
    return;
}

int main()
{
    int a, n;
    setlocale(LC_ALL, "ru");

    queue* q;
    q = (queue*)malloc(sizeof(queue));
    init(q);
    print(q);
    cout << "������� ������ �������: ";
    cin >> n;
    printf("\n");
    print(q);
    size(q);
    a = remove(q);
    print(q);
    size(q);
    clear(q);
    init(q);
    print(q);
    int menu = 0;
    do
    {
        cout << "......����......" << endl;
        cout << "1. ���-�� ���������." << endl;
        cout << "2. ������ ��� ���." << endl;
        cout << "3. �������� ������� � �������." << endl;
        cout << "4. ������� ������� �� �������." << endl;
        cout << "5. ������� �� ����� �������." << endl;
        cout << "6. ������� ���� �������." << endl;
        cout << "7. �����" << endl << endl;
        cin >> menu;
        switch (menu)
        {
        case 1:
            size(q);
            break;
        case 2:
            cout << isempty(q) << endl;
            break;
        case 3:
            for (int i = 0; i < n; i++)
            {
                printf("������� ������� �������: ");
                scanf_s("%d", &a);
                insert(q, a);
            }
            break;
        case 4:
            a = remove(q);
            printf("����� ������� %d\n", a);
            cout << endl;
            break;
        case 5:
            print(q);
            cout << endl;
            break;
        case 6:
            clear(q);
            init(q);
            break;
        }
    } while (menu != 7);
    system("PAUSE");
    return 0;
}*/

//2 �������

struct listok
{
    int key;
};

struct spisok
{
    listok element; // ���� ������
    spisok* next; // ��������� �� ��������� �������
};

void pereborelem(spisok* list)
{
    spisok* p;
    p = list;
    if (p != NULL)
    {
        do
        {
            cout << p->element.key << "  ";
            p = p->next;
        } 
        while (p);
    }

    cout << endl;
}

void init(spisok** begin)
{
    *begin = new spisok;

    listok massiv[10] = { 1, 4, 9, 16, 25, 36, 49, 64, 81, 100 };

    (*begin)->element.key = 0;
    (*begin)->next = NULL;

    spisok* end = *begin;

    for (int i(0); i < 10; i++)
    {
        end->next = new spisok;
        end = end->next;
        end->element = massiv[i];
        end->next = NULL;
    }
}

void addelem(spisok** begin, listok &element, listok &elem)//, listok&elem)
{
    spisok* index = new spisok;
    index->element = element;

    if (*begin == NULL)
    {
        index->next = NULL;
        *begin = index;
        return;
    }
    
    spisok* tpr = *begin;

    if (tpr->element.key > index->element.key)
    {
        index->next = NULL;
        *begin = index;
        return;
    }

    spisok* trl = tpr->next;


    while (trl)
    {
        if (trl->element.key == elem.key)
        {
            tpr->next = index;
            index->next = trl;
            return;
        }

        tpr = trl;
        trl = trl->next;
    }

    tpr->next = index;
    index->next = NULL;
}

void delelem(spisok** begin, listok& element)
{
    if (*begin == NULL)
    {
        return;
    }

    spisok* t = *begin;
    if (t->element.key == element.key)
    {
        *begin = t->next;
        delete t;
        return;
    }

    spisok* k = t->next;

    while (k)
    {
        if (k->element.key == element.key)
        {
            t->next = k->next;
            delete k;
            return;
        }
        t = k;
        k = k->next;
    }
}

void permunation(spisok** begin, int t1, int t2)
{
    spisok* found = *begin;
    spisok* right = *begin;

    while (found->element.key != t1)
    {
        found = found->next;
    }

    while (right->element.key != t2)
    {
        right = right->next;
    }

    spisok* temp = found;
    spisok* tmp = right;

    if (temp && tmp)
    {
        swap(temp->element.key, tmp->element.key);
    }
}

void print(spisok* lst)
{
    spisok* p;
    p = lst;
    if (p != NULL)
    {
        do
        {

            cout << p->element.key << " -> ";
            p = p->next;
        } while (p);
    }
    else
    {
        cout << "������ ����." << endl;
    }
    cout << "NULL\n";
}

void clear(spisok** begin)
{
    if (*begin == 0)
    {
        return;
    }

    spisok* prt = *begin;
    spisok* tut;

    while (prt)
    {
        tut = prt;
        prt = prt->next;
        delete tut;
    }
    *begin = NULL;
}


int main()
{
    setlocale(LC_ALL, "Russian");
    spisok* begin = NULL;

    int n, m, x, y, pos;

    listok number, num, numb;

    int menu = 0;
    do
    {
        cout << "......����......" << endl;
        cout << "1. ������� ���������." << endl;
        cout << "2. ������������ ������." << endl;
        cout << "3. �������� �������." << endl;
        cout << "4. ������� �������." << endl;
        cout << "5. ����������� ��������." << endl;
        cout << "6. ������� �� ����� ������." << endl;
        cout << "7. ������� ����� ������." << endl;
        cout << "8. �����" << endl << endl;


        cin >> menu;
        switch (menu)
        {
        case 1:
            pereborelem(begin);
            break;
        case 2:
            init(&begin);
            break;
        case 3:
            cout << "������� ������� ��� �������:\n";
            cin >> n;
            cout << "������� �������, �� ����� �������� ����� ��������� ��������� ������� ��� �������:\n";
            cin >> pos;
            number = { n };
            numb = { pos };
            addelem(&begin, number, numb);
            break;
        case 4:
            cout << "������� ������� ��� ��������:\n";
            cin >> m;
            num = { m };
            delelem(&begin, num);
            break;
        case 5:
            cout << "������� �������� ��� ������������:\n";
            cout << "1-� �������:\n";
            cin >> x;
            cout << "2-� �������:\n";
            cin >> y;
            permunation(&begin, x, y);
            cout << endl;
            break;
        case 6:
            print(begin);
            cout << endl;
            break;
        case 7:
            clear(&begin);
            break;
        }
    } while (menu != 8);

    system("PAUSE");
    return 0;
}